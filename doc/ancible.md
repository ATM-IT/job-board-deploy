# Installation de ANSIBLE sous MAC OS

```
brew install ansible
```
On peut voir le résultat de l'installation en tapant la commande suivante:

```
ansible --version
```

```
ansible 2.10.3
  config file = None
  configured module search path = ['/Users/newgate/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/local/Cellar/ansible/2.10.3_1/libexec/lib/python3.9/site-packages/ansible
  executable location = /usr/local/bin/ansible
  python version = 3.9.0 (default, Nov 21 2020, 14:01:50) [Clang 12.0.0 (clang-1200.0.32.27)]
  ```

  # Génération d'un clé SSH
  Ansible se base sur SSH pour envoyer ses commandes.
  1. Node root : le neoud dans lequel, il y a ansible installé.
  2. Node managed: Le/Les noeud(s) utilisée(s) pour l'installation.

  ## Commande de génération de la clé SSH
  
```
ssh-keygen -t ecdsa -b 521
```
Vous pouvez renseigner l'emplacement de la clé que vous allez générer
<br>A la fin du processus vous aller voir: 
<br>The key's randomart image is:

```
+---[ECDSA 521]---+
|.+o++..++oo      |
|. oo.o=oo. .     |
|.=oo+ooo .. o    |
|oO=.o+. o.Eo o   |
|*.ooo.. S*  o    |
|.+  ..  . +      |
|o        . .     |
|                 |
|                 |
+----[SHA256]-----+
```
