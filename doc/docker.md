# Docker commandes

## 1 - Création d'une image avec un fichier Dockerfile


```
docker build -t alpine-jdk11-openssh -f Dockerfile_jdk11_alpine_ssh .
```

1. Le nom du Dockerfile est Dockerfile_jdk11_alpine_ssh 

2. Le point à la fin est le chemin du fichier

3. L'option **-t** désigne le "TAG" ou l'identifiant de l'image

4. L'option **-f** désigne le nom du ficher Dockerfile utilisé

## 2 - Création d'un conteneur à partir d'une image

```
docker run --name mYcontainer alpine-jdk11-openssh
```

## 3 - Création d'un conteneur en mode détach **-d**

```
docker run -d --name mYcontainer alpine-jdk11-openssh
```

## 4 - Accés au conteneur 

```
docker exec -it mYcontainer bash
```

## 5 - Accès au données réseau de docker

```
docker network ls
```
Cette commande vous liste l'ensemble les networks de docker, il y a 3 types:

1. bridge
2. host
3. none

Par défaut tous les conteneurs lancés par docker se retrouvent sur le réseau bridge

```
 "Containers": {
            "208a377a5fe96ccdbc58692ce0095c582d94d2239597c8170cfae5d50d3ef478": {
                "Name": "myjenkins",
                "EndpointID": "df8063f855bd27b7cc048634f44a87a9fdf03e22da5182306d83295d33d88151",
                "MacAddress": "XX:XX:XX:XX:XX:XX",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "b063577cf4b9787b2ecc2c01999a12c31c8824cbdff97fd3e35d641918006d10": {
                "Name": "test2",
                "EndpointID": "a749350cd0aa8bba511743f079e2b20171f3c8bdea3760c81124de4f07790e5a",
                "MacAddress": "XX:XX:XX:XX:XX:XX",
                "IPv4Address": "172.17.0.4/16",
                "IPv6Address": ""
            },
            "f5e427f8df31483f2a2eb037059b021b843c1a68fac8df178f8015feab83beb1": {
                "Name": "mYcontainer",
                "EndpointID": "5f382e7ae81f85587ea3fef95efcdd793150c3a1bb3b8f96c90db946bce0451b",
                "MacAddress": "XX:XX:XX:XX:XX:XX",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
```